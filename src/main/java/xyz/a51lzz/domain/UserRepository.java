package xyz.a51lzz.domain;

import org.springframework.data.jpa.repository.JpaRepository;
import xyz.a51lzz.domain.entity.User;

/**
 * Created by lzz on 5/8/17.
 */
public interface UserRepository extends JpaRepository<User, Integer> {
}
