package xyz.a51lzz.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import xyz.a51lzz.domain.UserRepository;
import xyz.a51lzz.domain.entity.User;
import xyz.a51lzz.service.IUserService;

import java.util.List;

/**
 * Created by lzz on 5/8/17.
 */

@Service
public class UserServiceImpl implements IUserService {

    @Autowired
    private UserRepository mUserRepository;

    @Override
    public List<User> getList() {
        return mUserRepository.findAll();
    }

    @Override
    public User save(User user) {
        return mUserRepository.save(user);
    }

    @Override
    public void save(List<User> list) {
        mUserRepository.save(list);
    }
}
